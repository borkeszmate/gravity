import { Component, ViewChild, ElementRef, OnInit, NgZone } from '@angular/core';
import { Circle } from './objects/circle';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	@ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;
	ctx: CanvasRenderingContext2D;
	// xMiddle: number = Math.random() * window.innerWidth;
	yMiddle: number = window.innerHeight / 2;
	radius = 50;
	circleArr: Circle [] = [];
	colors = ['#05386b', '#379683', '#bcdb95', '#8ee4af', '#edf5e1'];

	mouse = {
		x: undefined,
		y: undefined
	};




	constructor(
		private ngZone: NgZone,
		) { }

	ngOnInit() {
		this.canvas.nativeElement.height = window.innerHeight;
		this.canvas.nativeElement.width = window.innerWidth;

		this.ctx = this.canvas.nativeElement.getContext('2d');
		this.update();
		for (let i = 0; i < 10; i++) {
			const x = Math.random() * window.innerWidth;
			const y = Math.random() * window.innerHeight;
			const color = this.colors[Math.floor(Math.random() * 5)];

			const circle = new Circle(x, y, this.radius, 0, 2 * Math.PI, 0 , 15, Math.random() * 100, 5, color, i.toString());
			this.circleArr = [...this.circleArr, circle];

			if (i > 0) {
				for ( let j = 0; j < this.circleArr.length; j++) {
					console.log(this.circleArr.length, j);
					const distance = this.getDistance(circle.x, this.circleArr[j].x, circle.y, this.circleArr[j].y);

					// if (distance < this.radius) {
					// 	console.log('correction');
					// 	this.circleArr[j].x = Math.random() * window.innerWidth;
					// 	this.circleArr[j].y = Math.random() * window.innerHeight;
					// 	i--;
					// }
				}
			}

		}

	}

	update() {
		this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

		this.ctx.fillStyle = 'grey';
		this.ctx.fillRect(0, 0, window.innerWidth, window.innerWidth);

		// tslint:disable-next-line: max-line-length
		const fixCircle = new Circle(window.innerWidth / 2 , window.innerHeight / 2, 150, 0 , 2 * Math.PI, undefined, undefined, undefined, undefined, 'red');
		fixCircle.draw();
		const mouseCircle = new Circle(this.mouse.x , this.mouse.y, 25, 0 , 2 * Math.PI, undefined, undefined, undefined, undefined, 'blue');

		requestAnimationFrame(() => {
			this.ngZone.runOutsideAngular(() => {
				this.update();
			});
			// circle.draw();
			for ( const circle of this.circleArr) {
				// circle.animate();
				circle.draw();
			}
			mouseCircle.draw();

			const distance = this.getDistance(fixCircle.x, mouseCircle.x, fixCircle.y, mouseCircle.y);
			// console.log(distance);

		});


	}

	getDistance(x1: number, x2: number, y1: number, y2: number) {
		const xDistance = x2 - x1;
		const yDistance = y2 - y1;
		const result = Math.pow( Math.pow(xDistance, 2) + Math.pow(yDistance, 2), 0.5);
		return result;
	}

	mouseMove(event) {
		this.mouse.x = event.x;
		this.mouse.y = event.y;
		// console.log(this.mouse);
	}


}
