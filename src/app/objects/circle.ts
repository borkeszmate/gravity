export class Circle {
	x: number;
	y: number;
	xMove: number;
	yMove: number;
	radius: number;
	sAngle: number;
	eAngle: number;
	slowDownLength: number;
	addedHeight = 0;
	slowDownSpeed: number;
	color: string;
	index: string;

	constructor(x, y, radius, sangle, eangle, xMove?, yMove?, slowDownLength?, slowDownSpeed?, color?, index? ) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.sAngle = sangle;
		this.eAngle = eangle;
		this.xMove = xMove;
		this.yMove = yMove;
		this.slowDownLength = slowDownLength;
		this.slowDownSpeed = slowDownSpeed;
		this.color = color;
		this.index = index;

	}


	canvas = document.querySelector('canvas');
	ctx = this.canvas.getContext('2d');

	draw() {
		this.ctx.fillStyle = this.color;
		this.ctx.beginPath();
		this.ctx.arc(this.x, this.y, this.radius, this.sAngle, this.eAngle);
		this.ctx.stroke();
		this.ctx.fill();
		// console.log(this.color);
		this.ctx.font = '30px Arial';
		this.ctx.fillStyle = 'black';
		this.ctx.fillText(this.index , this.x, this.y);
	}

	animate() {
		if ( this.y < window.innerHeight - this.radius  && this.yMove > 0) {
			this.yMove *= 1;
		}

		if (this.y > window.innerHeight - this.radius  && this.yMove > 0) {
			if (this.yMove - this.slowDownSpeed > 0) {
				this.yMove -= this.slowDownSpeed;
				this.addedHeight += this.slowDownLength;
				this.yMove *= -1;
			} else {
				this.yMove = 0;
			}
		}

		if (this.y < window.innerHeight / 2 + this.radius + this.addedHeight  && this.yMove < 0) {
			this.yMove *= -1;
			this.addedHeight += this.slowDownLength;
		}
		if (this.y === this.y + this.addedHeight + this.addedHeight * 0.2 && this.yMove < 0) {
			console.log(this.yMove);
			this.yMove = this.yMove / 1.2;
			console.log(this.yMove);
		}

		// if (this.y === this.y + this.addedHeight + this.addedHeight * 0.2 && this.yMove > 0) {
		// 	this.yMove = this.yMove * 1.2;
		// }

		// if (this.y === window.innerHeight - this.radius && this.yMove > 0) {
		// 	this.yMove *= -1;
		// }
		this.y += this.yMove;
		// console.log(this.yMove, window.innerHeight);
		this.draw();
	}

}
